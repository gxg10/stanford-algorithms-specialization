package com.company.h3;

import java.util.PriorityQueue;

public class HNode implements Comparable<HNode> {
    private String name;
    private int frequency;

    public HNode(String name, int frequency) {
        this.name = name;
        this.frequency = frequency;
    }

    @Override
    public String toString() {
        return "HNode{" +
                "name='" + name + '\'' +
                ", frequency=" + frequency +
                '}';
    }

    @Override
    public int compareTo(HNode o) {
        return frequency - o.frequency;
    }

    public static void main(String[] args) {
        int[] freq = {3, 2, 6, 8, 2, 6};

        PriorityQueue<HNode> queue = new PriorityQueue<>();
        for (int i: freq) {
            queue.offer(new HNode(String.valueOf(i), i));
        }

        System.out.println(queue);
    }
}

