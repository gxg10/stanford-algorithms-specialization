package com.company.huffman;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class HuffmanCode {

    private Map<Integer, String> mapVal = new HashMap<>();

    public Map<Integer, String> getMapVal() {
        return mapVal;
    }

    public void setMapVal(Map<Integer, String> mapVal) {
        this.mapVal = mapVal;
    }

    // input is an array of frequencies, indexed by character code
    public static HuffmanTree buildTree(Map<String, Integer> mapValori) {

        PriorityQueue<HuffmanTree> trees = new PriorityQueue<HuffmanTree>();
        // initially, we have a forest of leaves
        // one for each non-empty character
        for (int i = 0; i < charFreqs.length; i++)
            if (charFreqs[i] > 0)
                trees.offer(new HuffmanLeaf(charFreqs[i], (char)i));

            Set<String> keySet = mapValori.keySet();
            for (String s: keySet) {

            }

        assert trees.size() > 0;
        // loop until there is only one tree left
        while (trees.size() > 1) {
            // two trees with least frequency
            HuffmanTree a = trees.poll();
            HuffmanTree b = trees.poll();

            // put into new node and re-insert into queue
            trees.offer(new HuffmanNode(a, b));
        }
        return trees.poll();
    }

    public void printCodes(HuffmanTree tree, StringBuffer prefix) {

        assert tree != null;
        if (tree instanceof HuffmanLeaf) {
            HuffmanLeaf leaf = (HuffmanLeaf)tree;

            // print out character, frequency, and code for this leaf (which is just the prefix)
            this.mapVal.put(leaf.frequency, prefix.toString());
//            System.out.println(leaf.value + "\t" + leaf.frequency + "\t" + prefix);

        } else if (tree instanceof HuffmanNode) {
            HuffmanNode node = (HuffmanNode)tree;

            // traverse left
            prefix.append('0');
            printCodes(node.left, prefix);
            prefix.deleteCharAt(prefix.length()-1);

            // traverse right
            prefix.append('1');
            printCodes(node.right, prefix);
            prefix.deleteCharAt(prefix.length()-1);
        }
    }

    public static void main(String[] args) {
        String test = "this is an example for huffman encoding";

        // we will assume that all our characters will have
        // code less than 256, for simplicity
//        int[] charFreqs = new int[256];
//        // read each character and record the frequencies
//        for (char c : test.toCharArray())
//            charFreqs[c]++;

        // build tree
        int[] charFreqs = {10,
                74,
                46,
                25,
                48,
                13,
                37,
                97,
                77,
                45,
                96};
//        HuffmanTree tree = buildTree(charFreqs);

        // print out results
//        System.out.println("SYMBOL\tWEIGHT\tHUFFMAN CODE");

        Map<String, Integer> freq = new HashMap<>();

        StringBuilder sb = new StringBuilder();

        try (BufferedReader br = Files.newBufferedReader(Paths.get("input_random_2_10.txt"))) {
            String line;
            while ((line = br.readLine()) != null) {
                freq.put(line, Integer.valueOf(line));
                sb.append(line).append("\n");
            }
        } catch (IOException e) {

        }

        HuffmanTree tree = buildTree(freq);



        HuffmanCode code = new HuffmanCode();

        code.printCodes(tree, new StringBuffer());
//        System.out.println(code.getMapVal().entrySet().stream()
//        .max((e1, e2) -> e1.getValue().length() > e2.getValue().length() ?
//                1 : -1).get().getValue());
        DoubleSummaryStatistics sum = code.getMapVal().values()
                .stream().collect(Collectors.summarizingDouble(String::length));
        double max = sum.getMax();
        double min = sum.getMin();

        System.out.println("max code " + max + " min " + min);
    }
}