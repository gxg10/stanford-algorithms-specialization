package com.company.h2;

public class HuffmanNode implements Comparable<HuffmanNode> {

    int frequency;
    String data;
    HuffmanNode left, right;

    @Override
    public int compareTo(HuffmanNode o) {
        return frequency - o.frequency;
    }
}
