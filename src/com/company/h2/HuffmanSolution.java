package com.company.h2;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class HuffmanSolution {

    private static Map<String, String> charPrefixHashMap = new HashMap<>();
    static HuffmanNode root;

    public static void main(String[] args) {
        encode("aabc");
    }

    private static HuffmanNode buildTree(Map<String, Integer> freq) {
        PriorityQueue<HuffmanNode> priorityQueue =
                new PriorityQueue<>();

        Set<String> keySet = freq.keySet();

        for (String c: keySet) {
            HuffmanNode huffmanNode = new HuffmanNode();
            huffmanNode.data = c;
            huffmanNode.frequency = freq.get(c);
            huffmanNode.left = null;
            huffmanNode.right = null;

            priorityQueue.offer(huffmanNode);
        }

        while (priorityQueue.size() > 1) {
            HuffmanNode x = priorityQueue.peek();
            priorityQueue.poll();

            HuffmanNode y = priorityQueue.peek();
            priorityQueue.poll();

            HuffmanNode sum = new HuffmanNode();

            sum.frequency = x.frequency + y.frequency;
            sum.data = "-";
            sum.left = x;
            sum.right = y;

            root = sum;

            priorityQueue.offer(sum);
        }

        return priorityQueue.poll();
    }

    private static void setPrefixCodes(HuffmanNode node, StringBuilder prefix) {
        if (node != null) {
            if (node.left == null && node.right == null) {
                charPrefixHashMap.put(node.data, prefix.toString());
            } else {
                prefix.append('0');
                setPrefixCodes(node.left, prefix);
                prefix.deleteCharAt(prefix.length() - 1);

                prefix.append('1');
                setPrefixCodes(node.right, prefix);
                prefix.deleteCharAt(prefix.length() - 1);
            }
        }
    }

    private static void encode(String test) {
//        Map<Character, Integer> freq = new HashMap<>();
//        for (int i = 0 ; i < test.length(); i++) {
//            if (!freq.containsKey(test.charAt(i))) {
//                freq.put(test.charAt(i), 0);
//            } else {
//                freq.put(test.charAt(i), freq.get(test.charAt(i)) + 1);
//            }
//        }

        Map<String, Integer> freq = new HashMap<>();

        StringBuilder sb = new StringBuilder();

        try (BufferedReader br = Files.newBufferedReader(Paths.get("huffman.txt"))) {
            String line;
            while ((line = br.readLine()) != null) {
                freq.put(line, Integer.valueOf(line));
                sb.append(line).append("\n");
            }
        } catch (IOException e) {

        }

//        System.out.println(sb);

//        System.out.println("character freq map is " + freq);

        root = buildTree(freq);

        setPrefixCodes(root, new StringBuilder());
//        System.out.println("character prefix map = " + charPrefixHashMap);

        String maxCode = charPrefixHashMap.entrySet().stream()
                .max((e1, e2) -> e1.getValue().length() > e2.getValue().length() ? 1: -1).get().getValue();

        DoubleSummaryStatistics summaryStatistics = charPrefixHashMap.values().stream()
               .collect(Collectors.summarizingDouble(String::length));
        double max = summaryStatistics.getMax();
        double min = summaryStatistics.getMin();

        System.out.println("max code " + max + " min " + min);

//        for (int i = 0; i < test.length(); i++) {
//            char c = test.charAt(i);
//            s.append(charPrefixHashMap.get(c));
//        }
//        return s.toString();
    }

}
